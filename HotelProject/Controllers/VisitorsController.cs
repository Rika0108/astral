﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HotelProject.Data;
using HotelProject.Models;

namespace HotelProject.Controllers
{
    public class VisitorsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public VisitorsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Visitors
        public async Task<IActionResult> Index(string searchString)
        {
            if (!string.IsNullOrEmpty(searchString))
            {
                var filteredVisitors = _context.Visitors.Include(c => c.Room).Where(x => x.FullName.Contains(searchString)
                                                      || x.ArrivalDate.ToString("dd.MM.yyyy").Contains(searchString)
                                                      || x.DepartureDate.ToString().Contains(searchString));

                return View(filteredVisitors.ToList());
            }
            List<Visitor> visitors = _context.Visitors
                .Include(c => c.Room)
                .ToList();

            return View(visitors);
        }

        // GET: Visitors/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var visitor = await _context.Visitors.Include(c => c.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (visitor == null)
            {
                return NotFound();
            }

            return View(visitor);
        }

        // GET: Visitors/Create
        public IActionResult Create(Guid roomId)
        {
            ViewBag.roomID = roomId;
            return View();
        }

        // POST: Visitors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Guid roomId, [Bind("ID,FullName,ArrivalDate")] Visitor visitor)
        {
            if (ModelState.IsValid)
            {
                visitor.ID = Guid.NewGuid();
                visitor.Room = _context.Rooms.SingleOrDefault(x => x.ID == roomId);
                visitor.DepartureDate = null;
                _context.Add(visitor);
                _context.Rooms.Single(x => x.ID == roomId).IsBusy = true;
                _context.Update(_context.Rooms.Single(x => x.ID == roomId));

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(visitor);
        }

        // GET: Visitors/Edit/5
        public IActionResult Edit(Guid? roomId)
        {
            if (roomId == null)
            {
                return NotFound();
            }

            var visitor = _context.Visitors.Include(c => c.Room).SingleOrDefault(x => x.Room.ID == roomId && x.DepartureDate == null);
            if (visitor == null)
                return NotFound();


            visitor.DepartureDate = DateTime.UtcNow;
            visitor.Room.IsBusy = !visitor.Room.IsBusy;
            _context.Update(visitor);
            _context.SaveChangesAsync();

            double cost = 0;

            switch (visitor.Room.Type)
            {
                case RoomType.Standart:
                    cost += 100;
                    break;
                case RoomType.JuniorSuite:
                    cost += 200;
                    break;
                case RoomType.Lux:
                    cost += 300;
                    break;
            }

            cost += cost * 0.4 * (visitor.Room.NumberOfPlaces - 1);

            cost *= (visitor.DepartureDate - visitor.ArrivalDate).Value.Days;


            string result = $"Номер класса: {(typeof(RoomType).GetMember(visitor.Room.Type.ToString())[0].GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute)?.GetName()}; \r\n" +
                            $"Колличество мест - {visitor.Room.NumberOfPlaces}; \r\n" +
                            $"Колличество прожитых дней - {(visitor.DepartureDate - visitor.ArrivalDate).Value.Days}; \r\n" +
                            $"Полная стоимость проживания - {cost} у.е. \r\n";

            ViewBag.result = result;

            return View();
        }

       // GET: Visitors/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var visitor = await _context.Visitors
                .FirstOrDefaultAsync(m => m.ID == id);
            if (visitor == null)
            {
                return NotFound();
            }

            return View(visitor);
        }

        // POST: Visitors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var visitor = await _context.Visitors.FindAsync(id);
            _context.Visitors.Remove(visitor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VisitorExists(Guid id)
        {
            return _context.Visitors.Any(e => e.ID == id);
        }
    }
}