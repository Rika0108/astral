﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HotelProject.Models
{
    public class Room
    {
        public Guid ID { get; set; }
        [Display(Name = "Номер")]
        public int Number { get; set; }
        [Display(Name = "Тип")]
        public RoomType Type { get; set; }
        [Display(Name = "Места")]
        public int NumberOfPlaces { get; set; }
        [Display(Name = "Занятость")]
        public bool IsBusy { get; set; }

        public virtual ICollection<Visitor> Visitors { get; set; }
    }

    public enum RoomType
    {
        [Display(Name = "Стандартный")]
        Standart,
        [Display(Name = "Люкс")]
        Lux,
        [Display(Name = "Полулюкс")]
        JuniorSuite
    }

    public class RoomTypeViewModel
    {
        public RoomType Type { get; set; }
    }
}
