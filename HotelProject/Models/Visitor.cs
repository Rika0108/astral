﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HotelProject.Models
{
    public class Visitor
    {
        public Guid ID { get; set; }
        [Display(Name = "ФИО")]
        public string FullName { get; set; }
        [Display(Name = "Въезд")]
        [DataType(DataType.Date)]
        public DateTime ArrivalDate { get; set; }
        [Display(Name = "Выезд")]
        [DataType(DataType.Date)]
        public DateTime? DepartureDate { get; set; }

        public virtual Room Room { get; set; }
    }
}
