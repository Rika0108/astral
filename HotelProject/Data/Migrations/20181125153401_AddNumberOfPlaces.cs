﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelProject.Data.Migrations
{
    public partial class AddNumberOfPlaces : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NumberOfPlaces",
                table: "Rooms",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumberOfPlaces",
                table: "Rooms");
        }
    }
}
